// @ts-nocheck
// Core
import axios from 'axios';
import { ILoginFormShape } from '../components/forms/LoginForm/config';
import { ISignUpFormShape } from '../components/forms/SignUpForm/config';
import { ITaskModel } from '../types/TaskModel';
import { AuthHeader } from '../types/AuthHeader';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    getVersion() {
        return '0.0.1';
    },
    async login(credentials: ILoginFormShape) {
        const { email, password } = credentials;
        const { data } = await axios.get(
            `${TODO_API_URL}/auth/login`,
            {
                headers: {
                    authorization: `Basic ${btoa(`${email}:${password}`)}`,
                },
            },
        );

        return data;
    },
    async logout(token: string) {
        const { data } = await axios.get(
            `${TODO_API_URL}/auth/logout`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return data;
    },
    async signUp(credentials: ISignUpFormShape) {
        const { confirmPassword, ...body } = credentials;
        const { data } = await axios.post(
            `${TODO_API_URL}/auth/registration`,
            body,
        );

        return data;
    },
    async getProfile(token: string) {
        const { data } = await axios.get(
            `${TODO_API_URL}/auth/profile`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return data;
    },
    async getTags() {
        const { data: tags } = await axios.get(
            `${TODO_API_URL}/tags`,
        );

        return tags;
    },
    async getTasks() {
        const localToken = localStorage.getItem('token');
        const { data: tasks } = await axios.get<ITaskModel[]>(
            `${TODO_API_URL}/tasks`,
            {
                headers: {
                    authorization: `Bearer ${localToken}`,
                },
            },
        );

        return tasks;
    },
    async getTaskById(id: string) {
        const localToken = localStorage.getItem('token');
        const { data: taskById } = await axios.get<ITaskModel>(
            `${TODO_API_URL}/tasks/${id}`,
            {
                headers: {
                    authorization: `Bearer ${localToken}`,
                },
            },
        );

        return taskById;
    },
    async createTask(task: ITaskModel, token: string) {
        const config: AuthHeader = {};

        if (typeof token === 'string' && token) {
            config.headers = {
                authorization: `Bearer ${token}`,
            };
        }

        const { data: newTask } = await axios.post(
            `${TODO_API_URL}/tasks`,
            task,
            config,
        );

        return newTask;
    },
    async updateTask(task: ITaskModel) {
        const localToken = localStorage.getItem('token');

        const { data: currentTask } = await axios.put(
            `${TODO_API_URL}/tasks/${task.id}`,
            task,
            {
                headers: {
                    authorization: `Bearer ${localToken}`,
                },
            },
        );

        return currentTask;
    },
    async deleteTask(id: string) {
        const { data } = await axios.delete(
            `${TODO_API_URL}/tasks/${id}`,
            {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`,
                    id,
                },
            },
        );

        return data;
    },
});
