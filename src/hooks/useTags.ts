/* Core */
import { useQuery } from 'react-query';

/* Other */
import { api } from '../api';
import { ITagModel } from '../types/TagModel';

export const useTags = () => {
    const query = useQuery<ITagModel[]>('tags', api.getTags);

    return query;
};
