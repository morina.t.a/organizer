// @ts-nocheck

import { useDispatch } from 'react-redux';
import { useMutation } from 'react-query';
import { useEffect } from 'react';
import { authActions } from '../lib/redux/actions';
import { api } from '../api';

export const useUpdateTask = () => {
    const dispatch = useDispatch();
    const mutation = useMutation((data) => {
        return api.updateTask(data);
    },
    {
        onError: (error) => {
            const { response } = error;

            if (response?.status === 401) {
                dispatch(authActions.setError('Неверный логин или пароль. Проверьте корректность введённых данных.'));
            } else {
                dispatch(authActions.setError('Ошибка запроса. Повторите через несколько минут или обратитесь к администратору.'));
            }
        },
    });

    useEffect(async () => {
        if (mutation.isSuccess) {
            dispatch(authActions.setToken(mutation.data?.data));
            localStorage.setItem('token', mutation.data?.data);
            await profile.mutateAsync(mutation.data?.data);
            navigate('/');
        }
    }, [mutation.isSuccess]);

    return mutation;
};
