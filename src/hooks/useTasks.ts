/* Core */
import { useQuery } from 'react-query';

/* Other */
import { api } from '../api';
import { ITaskModel } from '../types/TaskModel';

export const useTasks = () => {
    const result = useQuery<ITaskModel[]>('tasks', api.getTasks);

    return result;
};
