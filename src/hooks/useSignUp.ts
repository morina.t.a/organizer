// @ts-nocheck
/* Core */
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useMutation } from 'react-query';
import { toast } from 'react-toastify';

/* Other */
import { api } from '../api';
import { ISignUpFormShape } from '../components/forms/SignUpForm/config';
import { authActions } from '../lib/redux/actions';
import { useProfile } from './useProfile';

export const useSignUp = () => {
    const profile = useProfile();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((credentials: ISignUpFormShape) => {
        return api.signUp(credentials);
    });

    useEffect(async () => {
        if (mutation.isSuccess) {
            const token = mutation.data?.data;

            dispatch(authActions.setToken(token));
            localStorage.setItem('token', token);
            await profile.mutateAsync(token);
            navigate('/todo/task-manager');

            const notify = () => toast.success('Добро пожаловать!', {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
        }
    }, [mutation.isSuccess]);

    return mutation;
};
