// @ts-nocheck
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useMutation } from 'react-query';
import { useEffect } from 'react';
import { authActions, mainActions } from '../lib/redux/actions';
import { api } from '../api';

export const useLogout = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((token: string) => {
        return api.logout(token);
    },
    {
        onError: (error) => {
            const { response } = error;
            if (response?.status === 401) {
                dispatch(authActions.setError('учётные данные не верны'));
            } else {
                dispatch(authActions.setError('необработанная ошибка на стороне сервера'));
            }
        },
    });
    useEffect(() => {
        if (mutation.isSuccess) {
            dispatch(authActions.setToken(null));
            dispatch(mainActions.setUser(null));
            localStorage.removeItem('token');
            navigate('/todo/login');

            const notify = () => toast.info('Возвращайтесь поскорее ;) Мы будем скучать.', {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
        }
    }, [mutation.isSuccess]);

    return mutation;
};
