/* Core */
import { useQuery } from 'react-query';

/* Other */
import { api } from '../api';
import { ITaskModel } from '../types/TaskModel';

export const useTaskById = (id: string) => {
    const query = useQuery<ITaskModel>(['tasks', id], () => api.getTaskById(id));

    return query;
};
