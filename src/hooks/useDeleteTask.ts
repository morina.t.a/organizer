// @ts-nocheck
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useMutation } from 'react-query';
import { authActions } from '../lib/redux/actions';
import { api } from '../api';

export const useDeleteTask = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((id) => {
        return api.deleteTask(id);
    },
    {
        onError: (error) => {
            const { response } = error;

            if (response?.status === 401) {
                dispatch(authActions.setError('Неверный логин или пароль. Проверьте корректность введённых данных.'));
            } else {
                dispatch(authActions.setError('Ошибка запроса. Повторите через несколько минут или обратитесь к администратору.'));
            }
        },
    });

    useEffect(() => {
        if (mutation.isSuccess) {
            const notify = () => toast.info('Задача удалена', {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            navigate('/');
        }
    }, [mutation.isSuccess]);

    return mutation;
};
