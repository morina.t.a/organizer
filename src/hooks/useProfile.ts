// @ts-nocheck
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useMutation } from 'react-query';
import { useEffect } from 'react';
import { authActions, mainActions } from '../lib/redux/actions';
import { api } from '../api';

export const useProfile = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((token: string) => {
        return api.getProfile(token);
    },
    {
        onError: (error) => {
            const { response } = error;

            if (response?.status === 404 || response?.status === 401) {
                navigate('/login');
            } else {
                dispatch(authActions.setError('Ошибка запроса. Повторите через несколько минут или обратитесь к администратору.'));
            }
        },
    });

    useEffect(async () => {
        if (mutation.isSuccess) {
            dispatch(mainActions.setUser(mutation?.data));
            navigate('/todo/task-manager');
            const { data } = await api.getTasks(localStorage.getItem('token'));
            dispatch(mainActions.setTasks(data));
        }
    }, [mutation.isSuccess]);

    return mutation;
};
