// @ts-nocheck
import { useMutation } from 'react-query';
import { api } from '../api';
import { ITaskModel } from '../types/TaskModel';

export const useCreateTask = () => {
    const token = localStorage.getItem('token');

    const mutation = useMutation(
        (task: ITaskModel) => {
            return api.createTask(task, token);
        },
    );

    return mutation;
};
