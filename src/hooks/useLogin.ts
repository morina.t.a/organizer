// @ts-nocheck
/* Core */
import { useEffect } from 'react';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { AxiosError } from 'axios';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useProfile } from './useProfile';


/* Other */
import { ILoginFormShape } from '../components/forms/LoginForm/config';
import { api } from '../api';
import { authActions } from '../lib/redux/actions';

export const useLogin = () => {
    const profile = useProfile();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((credentials: ILoginFormShape) => {
        return api.login(credentials);
    },
    {
        onError: (error: AxiosError) => {
            const { response } = error;

            if (response?.status === 401) {
                dispatch(authActions.setError('Неверный логин или пароль. Проверьте корректность введённых данных.'));
            } else {
                dispatch(authActions.setError('Ошибка запроса. Повторите через несколько минут или обратитесь к администратору.'));
            }
        },
    });

    useEffect(async () => {
        if (mutation.isSuccess) {
            dispatch(authActions.setToken(mutation.data?.data));
            localStorage.setItem('token', mutation.data?.data);
            await profile.mutateAsync(mutation.data?.data);
            navigate('/todo/task-manager');

            const notify = () => toast.success('Добро пожаловать!', {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
        }
    }, [mutation.isSuccess]);

    return mutation;
};
