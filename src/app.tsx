// @ts-nocheck
// Core
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ToastContainer, toast, Slide } from 'react-toastify';

// Components
import { Nav } from './components/Nav';
import { Main } from './components/Main';
import { Footer } from './components/Footer';
import { getErrorMessage } from './lib/redux/selectors';
import { authActions } from './lib/redux/actions';
import { useProfile } from './hooks/useProfile';

export const App: FC = () => {
    const dispatch = useDispatch();
    const errorMessage = useSelector(getErrorMessage);
    const profile = useProfile();

    useEffect(() => {
        if (errorMessage) {
            const notify = () => toast.error(errorMessage, {
                position:        'top-right',
                autoClose:       7000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            /**
             * необходимо очистить состояние ошибки что бы при повторном возникновении
             * такой же ошибки появилось вспылвающее сообщение
             * */
            dispatch(authActions.resetError());
        }
    }, [errorMessage]);

    const token = localStorage.getItem('token');
    useEffect(() => {
        if (token) {
            profile.mutateAsync(token);
        }
    }, []);

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />

            <Nav />
            <Main />
            <Footer />
        </>
    );
};
