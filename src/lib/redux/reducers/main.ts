// @ts-nocheck
import { mainTypes } from '../types';

const initialState = {
    user:  null,
    tags:  [],
    tasks: [],
    task:  {},
};

type MainState = typeof initialState;

export const mainReducer = (state : MainState = initialState, action): MainState => {
    switch (action.type) {
        case mainTypes.SET_USER: {
            return {
                ...state,
                user: action.payload,
            };
        }
        case mainTypes.SET_TAGS: {
            return {
                ...state,
                tags: action.payload,
            };
        }
        case mainTypes.SET_TASK: {
            return {
                ...state,
                tasks: [...state.tasks, action.payload],
            };
        }
        case mainTypes.SET_TASKS: {
            return {
                ...state,
                tasks: action.payload,
            };
        }
        case mainTypes.UPDATE_TASK_BY_ID: {
            return {
                ...state,
                tasks: state.tasks.map((task) => {
                    return task.id === action.payload.id ? action.payload : task;
                }),
            };
        }
        case mainTypes.DELETE_TASK_BY_ID: {
            return {
                ...state,
                tasks: state.tasks.filter((task) => task.id === action.payload),
            };
        }
        default: {
            return state;
        }
    }
};
