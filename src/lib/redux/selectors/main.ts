import { RootReducerState } from '../init/rootReducer';

export const getUser = (state: RootReducerState) => {
    return state.main.user;
};

export const getTags = (state: RootReducerState) => {
    return state.main.tags;
};

export const getTasks = (state: RootReducerState) => {
    return state.main.tasks;
};
