// @ts-nocheck
import { mainTypes } from '../types';

export const mainActions = Object.freeze({
    setUser: (data) => ({
        type:    mainTypes.SET_USER,
        payload: data,
    }),
    setTags: (data) => ({
        type:    mainTypes.SET_TAGS,
        payload: data,
    }),
    setTask: (data) => ({
        type:    mainTypes.SET_TASK,
        payload: data,
    }),
    setTasks: (data) => ({
        type:    mainTypes.SET_TASKS,
        payload: data,
    }),
    updateTaskById: (data) => ({
        type:    mainTypes.UPDATE_TASK_BY_ID,
        payload: data,
    }),
    deleteTaskById: (data) => ({
        type:    mainTypes.DELETE_TASK_BY_ID,
        payload: data,
    }),
});
