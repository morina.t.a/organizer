// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth,
    mainReducer as main,
} from '../reducers';

export const rootReducer = combineReducers({
    auth,
    main,
});

export type RootReducerState = ReturnType<typeof rootReducer>;
