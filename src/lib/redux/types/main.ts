export const mainTypes = Object.freeze({
    SET_USER:          'SET_USER',
    SET_TAGS:          'SET_TAGS',
    SET_TASK:          'SET_TASK',
    SET_TASKS:         'SET_TASKS',
    UPDATE_TASK_BY_ID: 'UPDATE_TASK_BY_ID',
    DELETE_TASK_BY_ID: 'DELETE_TASK_BY_ID',
});
