export enum TagEnum {
    SKETCH = 'Sketch',
    SPOTIFY = 'Spotify',
    DRIBBLE = 'Dribble',
    BEHANCE = 'Behance',
    UX = 'UX',
}

export interface ITagModel {
    id: string;
    bg: string;
    color: string;
    name: TagEnum;
}
