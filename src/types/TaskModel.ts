export interface ITaskModel {
    id: string;
    title: string;
    deadline: string;
    description: string;
    tag: {
        id: string;
        name: string;
        bg: string;
        color: string;
    };
    created: string;
    completed: boolean;
}
