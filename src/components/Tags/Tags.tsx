// @ts-nocheck
/* Core */
import { FC, useEffect } from 'react';

/* Components */
import { Tag } from './Tag';

/* Other */
import { useTags } from '../../hooks/useTags';
import { ITagModel } from '../../types/TagModel';

export const Tags: FC<ITagModel[]> = ({
    form, selectedTagId, setSelectedTagId, selectedTask,
}) => {
    const { data: tags, isFetchedAfterMount } = useTags();

    useEffect(() => {
        if (!selectedTask && !selectedTagId && Array.isArray(tags)) {
            setSelectedTagId(tags[ 0 ]?.id);
        }
        if (selectedTask) {
            setSelectedTagId(selectedTask.tag.id);
        }
    }, [isFetchedAfterMount, selectedTask]);

    const handleTagClick = (id: string) => {
        setSelectedTagId(id);
        form.setValue('tag', id, { shouldDirty: true });
    };

    const tagsJSX = tags?.map((tag) => (
        <Tag
            key = { tag.id }
            { ...tag }
            dataActive = { selectedTagId === tag.id }
            handleTagClick = { handleTagClick } />
    ));

    return <div className = 'tags'>{ tagsJSX }</div>;
};
