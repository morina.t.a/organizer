/* Core */
import { FC } from 'react';

/* Other */
import { ITagModel } from '../../types/TagModel';

export const Tag: FC<IPropTypes> = ({
    id, name, bg, color, dataActive, handleTagClick,
}) => {
    const selectedId = dataActive ? 'tag selected' : 'tag';

    return (
        <span
            style = { { backgroundColor: bg, color } }
            data-active = { dataActive }
            onClick = { () => handleTagClick(id) }
            className = { selectedId } >
            { name }
        </span>
    );
};

interface IPropTypes extends ITagModel {
    dataActive: boolean;
    handleTagClick: (value: string) => void;
}
