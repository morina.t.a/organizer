// @ts-nocheck
import { Navigate, Route, Routes } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getUser } from '../lib/redux/selectors';
import { MainWrapper } from './styles/Main.styles';
import { LoginForm, SignUpForm } from './forms';
import { TaskManager } from './TaskManager';
import { Profile } from './Profile';

export const Main = () => {
    const user = useSelector(getUser);

    return (
        <MainWrapper>
            <Routes>
                { user && <Route path = '/todo/task-manager' element = { <TaskManager /> } /> }
                <Route path = '/todo/login' element = { <LoginForm /> } />
                <Route path = '/todo/signup' element = { <SignUpForm /> } />
                <Route path = '/todo/profile' element = { <Profile /> } />

                <Route path = '*' element = { <Navigate to = { user ? '/todo/task-manager' : '/todo/login' } />  } />
            </Routes>
        </MainWrapper>
    );
};
