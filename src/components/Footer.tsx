import { FooterWrapper } from './styles/Footer.slyles';

export const Footer = () => {
    return (
        <FooterWrapper>
            <span>© 2021 Lectrum LLC - Все права защищены.</span>
        </FooterWrapper>
    );
};
