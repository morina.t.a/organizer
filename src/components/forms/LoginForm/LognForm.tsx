/* Core */
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

/* Components */
import { Input } from '../elements';

/* Other */
import { loginSchema, ILoginFormShape } from './config';
import { FormWrapper } from '../../styles/Form.styles';
import { useLogin } from '../../../hooks';

export const LoginForm = () => {
    const auth = useLogin();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(loginSchema),
    });

    const login = form.handleSubmit(async (credentials: ILoginFormShape) => {
        await auth.mutateAsync(credentials);
        form.reset();
    });

    return (
        <FormWrapper className = 'publish-tip'>
            <form onSubmit = { login }>
                <fieldset disabled = { auth.isLoading }>
                    <legend>Вход</legend>
                    <Input
                        label = ''
                        placeholder = 'Электропочта'
                        error = { form.formState.errors.email }
                        register = { form.register('email') } />
                    <Input
                        label = ''
                        placeholder = 'Пароль'
                        type = 'password'
                        error = { form.formState.errors.password }
                        register = { form.register('password') } />

                    <input
                        className = 'button-login'
                        type = 'submit'
                        value = 'Войти' />
                </fieldset>
                <p>
                    Если у вас до сих пор нет учётной записи, вы можете{ ' ' }
                    <Link to = '/todo/signup'>зарегистрироваться</Link>.
                </p>
            </form>
        </FormWrapper>
    );
};
