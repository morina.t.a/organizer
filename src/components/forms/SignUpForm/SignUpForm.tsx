/* Core */
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

/* Components */
import { Input } from '../elements';

/* Other */
import { ISignUpFormShape, signUpFormSchema } from './config';
import { FormWrapper } from '../../styles/Form.styles';
import { useSignUp } from '../../../hooks';

export const SignUpForm = () => {
    const auth = useSignUp();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(signUpFormSchema),
    });

    const signUp = form.handleSubmit(async (credentials: ISignUpFormShape) => {
        await auth.mutateAsync(credentials);
        form.reset();
    });

    return (
        <FormWrapper className = 'publish-tip'>
            <form onSubmit = { signUp }>
                <fieldset disabled = { auth.isLoading }>
                    <legend>Регистрация</legend>
                    <Input
                        label = ''
                        placeholder = 'Имя и фамилия'
                        error = { form.formState.errors.name }
                        register = { form.register('name') } />

                    <Input
                        label = ''
                        placeholder = 'Электропочта'
                        error = { form.formState.errors.email }
                        register = { form.register('email') } />
                    <Input
                        label = ''
                        placeholder = 'Пароль'
                        type = 'password'
                        error = { form.formState.errors.password }
                        register = { form.register('password') } />
                    <Input
                        label = ''
                        placeholder = 'Подтверждение пароля'
                        type = 'password'
                        error = { form.formState.errors.confirmPassword }
                        register = { form.register('confirmPassword') } />

                    <input
                        className = 'button-login'
                        type = 'submit'
                        value = 'Зарегистрироваться' />
                </fieldset>
                <p>
                    Перейти к <Link to = '/todo/login'>логину</Link>.
                </p>
            </form>
        </FormWrapper>
    );
};
