/* Core */
import * as yup from 'yup';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'Минимальная длина поля ${label} — ${min} символов';
// eslint-disable-next-line no-template-curly-in-string
const tooLongMessage = 'максимальная длина ${label} — ${max} символов';

export const taskCardSchema: yup.SchemaOf<ITaskCardShape> = yup.object().shape({
    title: yup
        .string()
        .label('title')
        .min(3, tooShortMessage)
        .max(64, tooLongMessage)
        .required('Минимальная длина поля title — 3'),
    deadline: yup
        .date()
        .required(),
    description: yup
        .string()
        .label('description')
        .min(3, tooShortMessage)
        .max(64, tooLongMessage)
        .required('Минимальная длина поля description — 3'),
});

/* Types */
export interface ITaskCardShape {
    title: string;
    description: string;
}
