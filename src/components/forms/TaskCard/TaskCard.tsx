// @ts-nocheck
import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import DatePicker from 'react-datepicker';
import { format, parseISO } from 'date-fns';
import { ru } from 'date-fns/locale';
import { yupResolver } from '@hookform/resolvers/yup';

import { mainActions } from '../../../lib/redux/actions';
import { taskCardSchema } from './config';
import { useCreateTask, useUpdateTask, useDeleteTask } from '../../../hooks';

import { Tags } from '../../Tags';
import { ITaskModel } from '../../../types/TaskModel';

export const TaskCard: FC = ({ setShowTask, selectedTask, setTaskComplete }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const createTask = useCreateTask();
    const updateTask = useUpdateTask();
    const deleteTask = useDeleteTask();
    const [selectedTagId, setSelectedTagId] = useState();
    const [startDate, setStartDate] = useState(new Date());
    const day = format(startDate, 'dd MMM yyyy', { locale: ru });

    const deadlineDay = selectedTask && format(parseISO(selectedTask.deadline), 'd MMM yyyy', { locale: ru });

    const form = useForm({
        defaultValues: {
            title:       '',
            deadline:    startDate,
            description: '',
            tag:         '',
        },
        mode:     'onTouched',
        resolver: yupResolver(taskCardSchema),
    });

    const submitForm = form.handleSubmit(async (data: ITaskModel) => {
        if (selectedTask?.id) {
            const updatedTask = await updateTask.mutateAsync({
                ...selectedTask,
                ...data,
                deadline: startDate,
                tag:      selectedTagId,
            });
            dispatch(mainActions.updateTaskById(updatedTask));
            const notify = () => toast.info(`Задача с идентификатором ${selectedTask?.id} успешно обновлена.`, {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            navigate('/');
        } else {
            await createTask.mutateAsync({
                ...data,
                deadline: startDate,
                tag:      selectedTagId,
            });
        }
        setShowTask(false);
        form.reset();
        navigate('/');
    });

    const resetForm = form.handleSubmit(() => {
        form.reset();
    });

    const handleCompleteButton = () => {
        setTaskComplete(true);

        const notify = () => toast.info(`Задача с идентификатором ${selectedTask?.id} успешно завершена.`, {
            position:        'top-right',
            autoClose:       5000,
            hideProgressBar: false,
            closeOnClick:    true,
            pauseOnHover:    true,
            draggable:       true,
            progress:        undefined,
        });
        notify();
    };

    const handleRemoveButton = async () => {
        if (selectedTask?.id) {
            await deleteTask.mutateAsync(selectedTask?.id);
            dispatch(mainActions.deleteTaskById(selectedTask?.id));
            form.reset();
        }
    };

    return (
        <div className = 'task-card'>
            <form onSubmit = { submitForm } onReset = { resetForm } >
                <div className = 'head'>
                    <button
                        className = 'button-complete-task'
                        disabled = { !selectedTask }
                        onClick = { handleCompleteButton } >
                        завершить
                    </button>
                    { selectedTask && <div className = 'button-remove-task' onClick = { handleRemoveButton } /> }
                </div>
                <div className = 'content'>
                    <label className = 'label'>
                        Задачи
                        <input
                            // value = { selectedTask?.title || '' }
                            className = 'title'
                            placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                            type = 'text'
                            error = { form.formState.errors.title }
                            { ...form.register('title') } />
                    </label>
                    <div className = 'deadline'>
                        <span className = 'label'>
                            Дедлайн
                        </span>
                        <span className = 'date'>
                            <DatePicker
                                // value = { deadlineDay || null }
                                name = 'deadline'
                                selected = { startDate }
                                minDate = { new Date() }
                                dateFormat = { day }
                                onChange = { (date) => {
                                    setStartDate(date);
                                    form.setValue('deadline', date, { shouldDirty: true });
                                } } />
                        </span>
                    </div>
                    <div className = 'description'>
                        <label className = 'label'>
                            Описание
                            <textarea
                                // value = { selectedTask?.description || '' }
                                className = 'text'
                                placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.'
                                error = { form.formState.errors.description }
                                { ...form.register('description') } />
                        </label>
                    </div>
                    <Tags
                        form = { form }
                        selectedTagId = { selectedTagId }
                        setSelectedTagId = { setSelectedTagId }
                        selectedTask = { selectedTask } />
                    <div className = 'errors'>
                        { form.formState.isDirty && form.formState.errors?.title && <p className = 'errorMessage'>{ form.formState.errors?.title?.message }</p> }
                        { form.formState.isDirty && form.formState.errors?.description && <p className = 'errorMessage'>{ form.formState.errors?.description?.message }</p> }
                    </div>
                    <div className = 'form-controls'>
                        <button
                            type = 'reset'
                            className = 'button-reset-task'
                            disabled = { !form.formState.isDirty }>
                            Reset
                        </button>
                        <button
                            type = 'submit'
                            className = 'button-save-task'
                            disabled = { !form.formState.isDirty }>
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};
