// @ts-nocheck
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { getTasks } from '../lib/redux/selectors';

import { TaskCard } from './forms/TaskCard';
import { TaskList } from './TaskList';

export const TaskManager = () => {
    const [showTask, setShowTask] = useState(false);
    const [selectedTask, setSelectedTask] = useState();
    const [taskComplete, setTaskComplete] = useState(false);
    const tasks = useSelector(getTasks);

    const handleClick = () => {
        setShowTask(true);
        setSelectedTask(null);
    };

    return (
        <>
            <div className = 'controls'>
                <i className = 'las' />
                <button
                    onClick = { handleClick }
                    className = 'button-create-task'>
                    Новая задача
                </button>
            </div>
            <div className = 'wrap'>
                <div className = { `list ${tasks.length ? '' : 'empty'}` }>
                    <TaskList
                        setShowTask = { setShowTask }
                        setSelectedTask = { setSelectedTask }
                        taskComplete = { taskComplete } />
                </div>
                { showTask
                    && <TaskCard
                        setShowTask = { setShowTask }
                        selectedTask = { selectedTask }
                        setTaskComplete = { setTaskComplete } /> }
            </div>
        </>
    );
};
