
import styled from 'styled-components';

export const NavWrapper = styled.nav`
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 30px 40px;

    a {
        text-decoration: unset;
        margin-left: 30px;
        color: $color2;
        font-family: 'Roboto', sans-serif;
        font-weight: 200;
        font-size: 18px;

        &:hover {
            color: $color13;
        }
        &.active {
            color: $color3;
            font-weight: 700;
        }
        &[aria-disabled=true] {
             cursor: not-allowed;
        }
    }

    .button-logout {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 9px 19px 8px;
        border: none;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: bold;
        background: none;
        color: #ffffff;
        border-radius: 4px;
        transition: all 200ms;
        transform: scale(1);
        background-color: #d8d8d8;
        margin-left: 50px;

        &:hover {
            cursor: pointer;
            transform: scale(1.1);
            background-color: $color11;
            color: white;


        }
    }
`;
