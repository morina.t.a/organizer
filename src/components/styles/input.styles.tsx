
import styled from 'styled-components';

export const InputWrapper = styled.label`
    display: block;
    width: 100%;
    margin-bottom: 15px;
    color: $color2;

    .errorMessage {
        color: #8d211d;
        margin-bottom: 5px;
    }

    input {
        width: 100%;
        border: none;
        border-bottom: 1px solid #ececec;
        padding: 10px 0;
        outline: none;
        color: #252631;
        font-size: 20px;
        margin-bottom: 5px;

        &::-webkit-input-placeholder {
            color: #98a9bc;
        }

        &:-ms-input-placeholder {
            color: #98a9bc;
        }

        &::placeholder {
            color: #98a9bc;
        }

        &:active,
        &:focus {
            outline: unset;
        }
    }
`;
