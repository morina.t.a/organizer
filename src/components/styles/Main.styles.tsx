
import styled from 'styled-components';

export const MainWrapper = styled.main`
    padding-top: 87px;
    min-height: 800px;

    .sign-form: {
        width: calc(40% - 45px);
        margin: 0 auto;

        form {
            background-color: white;
            border-radius: 10px;
            padding: 50px 40px;
        }

        fieldset{
            border: unset;
        }

        fieldset + p {
            margin-top: 40px;
            font-size: 14px;
            color: $color2;
            font-family: 'Roboto', sans-serif;
            font-weight: 200;

            a{
                text-decoration: unset;
                color: $color3;
            }
        }

        legend {
            text-align: center;
            font-size: 24px;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            margin-bottom: 20px;
            color: $color17;

        }

        label {
            display: block;
            width: 100%;
            margin-bottom: 15px;
            color: $color2;

            .errorMessage {
                color: #8D211D;
                margin-bottom: 5px;
            }

            input {
                width: 100%;
                border: none;
                border-bottom: 1px solid #ececec;
                padding: 10px 0;
                outline: none;
                color: #252631;
                font-size: 20px;
                margin-bottom: 5px;

                &::-webkit-input-placeholder {
                    color: #98a9bc;
                }

                &:-ms-input-placeholder {
                    color: #98a9bc;
                }

                &::placeholder {
                    color: #98a9bc;
                }

                &:active,
                &:focus {
                    outline: unset;
                }
            }
        }

        [type="submit"] {
            margin-right: 10px;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 9px 19px 8px;
            border: none;
            font-size: 14px;
            text-transform: uppercase;
            font-weight: bold;
            background: none;
            color: $color6;
            border-radius: 4px;
            transition: all 200ms;

            &:hover {
                cursor: pointer;
                transform: scale(1.1);
            }
        }

        .button-login {
            background-color: $color13;

            &:disabled {
                cursor: initial;
                transform: scale(1);
                filter: grayscale(100%);
                opacity: 0.3;
            }
        }

        .controls {
            display: flex;
            justify-content: flex-end;
            padding-right: 37px;
        }
    }
`;
