
import styled from 'styled-components';

export const FooterWrapper = styled.footer`
    position: unset;
    display: flex;
    justify-content: center;
    padding: 20px;

    span {
        font-size: 14px;
        color: #98A9BC;
        text-align: center;
    }
`;
