
import styled from 'styled-components';

export const FormWrapper = styled.section`
    width: calc(40% - 45px);
    margin: 0 auto;

    form {
        background-color: white;
        border-radius: 10px;
        padding: 50px 40px;
    }

    fieldset{
        border: unset;
    }

    fieldset + p {
        margin-top: 40px;
        font-size: 14px;
        color: #98a9bc;
        font-family: 'Roboto', sans-serif;
        font-weight: 200;

        a {
            text-decoration: unset;
            color: #4d7cfe;
        }
    }

    legend {
        text-align: center;
        font-size: 24px;
        font-family: 'Roboto', sans-serif;
        font-weight: 500;
        margin-bottom: 20px;
        color: #778ca2;
    }

    [type="submit"] {
        margin-right: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 9px 19px 8px;
        border: none;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: bold;
        background: none;
        color: #fff;
        border-radius: 4px;
        transition: all 200ms;

        &:hover {
            cursor: pointer;
            transform: scale(1.1);
        }
    }

    .button-login {
        background-color: #4d7cfe;

        &:disabled {
            cursor: initial;
            transform: scale(1);
            filter: grayscale(100%);
            opacity: 0.3;
        }
    }
`;
