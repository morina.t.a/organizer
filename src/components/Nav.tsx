// @ts-nocheck
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { NavWrapper } from './styles/Nav.styles';
import { getUser } from '../lib/redux/selectors';
import { useLogout } from '../hooks/useLogout';

export const Nav = () => {
    const user = useSelector(getUser);
    const auth = useLogout();

    const handleLogout =  async (event: never) => {
        event.preventDefault();
        await auth.mutateAsync(localStorage.getItem('token'));
    };

    return (
        <NavWrapper>
            { user && <>
                <NavLink to = '/todo/task-manager'>
                    К задачам
                </NavLink>
                <NavLink to = '/todo/profile'>
                    Профиль
                </NavLink>
                <button className = 'button-logout' onClick = { handleLogout }>Выйти</button>
            </>
            }
        </NavWrapper>
    );
};
