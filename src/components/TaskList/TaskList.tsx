// @ts-nocheck
/* Core */
import { FC, useEffect, useState } from 'react';

/* Components */
import { Task } from './Task';

/* Other */
import { useTasks } from '../../hooks';

export const TaskList: FC = ({ setShowTask, setSelectedTask, taskComplete }) => {
    const [selectedTaskId, setSelectedTaskId] = useState();
    const queryTasks = useTasks();
    const tasks = queryTasks?.data?.data;

    const handleTaskClick = (id: string) => {
        setSelectedTaskId(id);
        setShowTask(true);
    };

    const selectedTask = tasks?.find((item) => item.id === selectedTaskId);

    useEffect(() => {
        setSelectedTask(selectedTask);
    }, [selectedTaskId]);

    const tasksJSX = tasks?.map((task) => (
        <Task
            key = { task.id }
            { ...task }
            handleTaskClick = { handleTaskClick }
            dataActive = { selectedTaskId === task.id  }
            selectedTaskId = { selectedTaskId }
            taskComplete = { taskComplete } />
    ));

    return <div className = 'tasks'>{ tasksJSX }</div>;
};
