// @ts-nocheck
/* Core */
import { FC } from 'react';
import { format, parseISO } from 'date-fns';
import { ru } from 'date-fns/locale';

/* Other */
import { ITaskModel } from '../../types/TaskModel';

export const Task: FC<ITaskModel> = (props) => {
    const {
        id, title, deadline, tag, handleTaskClick, completed,
    } = props;

    const deadlineDay = format(parseISO(deadline), 'd MMM yyyy', { locale: ru });

    return (
        <div
            className = { `task ${completed ? 'completed' : ''}` }
            onClick = { () => handleTaskClick(id) } >
            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ deadlineDay }</span>
                <span
                    style = { { backgroundColor: tag.bg, color: tag.color } }
                    className = 'tag'>{ props.tag.name }</span>
            </div>
        </div>
    );
};
